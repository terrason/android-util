package org.terramaget.android.apkinfo;

public class ApkInfo {

    private String packageName;
    private String versionName;
    private String versionCode;

    public ApkInfo() {
    }

    public ApkInfo(String packageName, String versionName, String versionCode) {
	this.packageName = packageName;
	this.versionName = versionName;
	this.versionCode = versionCode;
    }

    public String getPackageName() {
	return packageName;
    }

    public void setPackageName(String packageName) {
	this.packageName = packageName;
    }

    public String getVersionName() {
	return versionName;
    }

    public void setVersionName(String versionName) {
	this.versionName = versionName;
    }

    public String getVersionCode() {
	return versionCode;
    }

    public void setVersionCode(String versionCode) {
	this.versionCode = versionCode;
    }

    public String toString() {
	return (new StringBuilder("ApkInfo [packageName=")).append(packageName).append(", versionName=").append(versionName).append(", versionCode=").append(versionCode).append("]").toString();
    }
}
