package org.terramaget.android.apkinfo;

/**
 * 获取APK信息.
 *
 */
public class GetApkInfo {

    public static void main(String[] args) throws Exception {
	if (args == null || args.length == 0) {
	    System.out.println("Usage: GetApkInfo <Android apk file>");
	    return;
	}
	ApkUtil util = new ApkUtil();
	ApkInfo apkInfo = util.getApkInfo(args[0]);
	System.out.println(apkInfo);
    }
}
