/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramaget.android.apkinfo;

import android.content.res.AXmlResourceParser;
import android.util.TypedValue;
import java.io.InputStream;
import java.util.IllegalFormatException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.xmlpull.v1.XmlPullParser;

/**
 *
 * @author lip
 */
public class ApkUtil {

    public ApkInfo getApkInfo(String apkpath) throws Exception {
	ZipFile zFile = null;
	ApkInfo apk = null;
	try {
	    apk = new ApkInfo();
	    zFile = new ZipFile(apkpath);
	    ZipEntry entry = zFile.getEntry("AndroidManifest.xml");
	    if (entry == null) {
		throw new IllegalArgumentException("apk文件格式不正确");
	    }
	    InputStream inputStream = zFile.getInputStream(entry);
	    AXmlResourceParser parser = new AXmlResourceParser();
	    parser.open(inputStream);
	    int type;
	    while ((type = parser.next()) != XmlPullParser.END_DOCUMENT) {
		if (type == XmlPullParser.START_TAG) {
		    for (int i = 0; i != parser.getAttributeCount(); ++i) {
			if ("versionName".equals(parser.getAttributeName(i))) {
			    apk.setVersionName(getAttributeValue(parser, i));
			} else if ("package".equals(parser.getAttributeName(i))) {
			    apk.setPackageName(getAttributeValue(parser, i));
			} else if ("versionCode".equals(parser.getAttributeName(i))) {
			    apk.setVersionCode(getAttributeValue(parser, i));
			}
		    }
		    break;
		}
	    }
	} finally {
	    if (zFile != null) {
		zFile.close();
	    }
	}
	return apk;
    }

    private String getAttributeValue(AXmlResourceParser parser, int index) {
	int type = parser.getAttributeValueType(index);
	int data = parser.getAttributeValueData(index);
	if (type == TypedValue.TYPE_STRING) {
	    return parser.getAttributeValue(index);
	}
	if (type == TypedValue.TYPE_ATTRIBUTE) {
	    return String.format("?%s%08X", getPackage(data), data);
	}
	if (type == TypedValue.TYPE_REFERENCE) {
	    return String.format("@%s%08X", getPackage(data), data);
	}
	if (type == TypedValue.TYPE_FLOAT) {
	    return String.valueOf(Float.intBitsToFloat(data));
	}
	if (type == TypedValue.TYPE_INT_HEX) {
	    return String.format("0x%08X", data);
	}
	if (type == TypedValue.TYPE_INT_BOOLEAN) {
	    return data != 0 ? "true" : "false";
	}
	if (type == TypedValue.TYPE_DIMENSION) {
	    return Float.toString(complexToFloat(data))
		    + DIMENSION_UNITS[data & TypedValue.COMPLEX_UNIT_MASK];
	}
	if (type == TypedValue.TYPE_FRACTION) {
	    return Float.toString(complexToFloat(data))
		    + FRACTION_UNITS[data & TypedValue.COMPLEX_UNIT_MASK];
	}
	if (type >= TypedValue.TYPE_FIRST_COLOR_INT && type <= TypedValue.TYPE_LAST_COLOR_INT) {
	    return String.format("#%08X", data);
	}
	if (type >= TypedValue.TYPE_FIRST_INT && type <= TypedValue.TYPE_LAST_INT) {
	    return String.valueOf(data);
	}
	return String.format("<0x%X, type 0x%02X>", data, type);
    }

    private String getPackage(int id) {
	if (id >>> 24 == 1) {
	    return "android:";
	}
	return "";
    }

    private float complexToFloat(int complex) {
	return (float) (complex & 0xFFFFFF00) * RADIX_MULTS[(complex >> 4) & 3];
    }

    private static final float RADIX_MULTS[] = {0.00390625F, 3.051758E-005F, 1.192093E-007F, 4.656613E-010F};
    private static final String DIMENSION_UNITS[] = {"px", "dip", "sp", "pt", "in", "mm", "", ""};
    private static final String FRACTION_UNITS[] = {"%", "%p", "", "", "", "", "", ""};
}
